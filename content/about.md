---
title: "Desafio de Inteligência Articial com Cerveja"
description: "Sobre Desafio de Inteligência Articial com Cerveja"
date: "2017-01-14"
categories: 
    - "Desafios"
---

![Cover](/logo_500.jpg#centered)

## Proposta

A proposta é formar um grupo de estudo para aprender e compartilhar conhecimentos sobre **inteligência artificial**. 
E com certa frequência praticar os conhecimentos adquiridos em um **desafio prático** e **divertido**. Esse desafio será apresentado com
antecedência e os participantes irão propor uma **solução**. Em um dia e local determinados, a competição acontecerá.
As regras podem variar. O que não varia é o formato: A competição é ***presencial***, com uma tela para assistir o desenrolar dos desafios
e **apreciar a cerveja e companhia dos participantes**.

![Party](/about/party_demo.jpg#centered)
<p style="text-align: center">Imagem meramente demonstrativa :p</p>

### Como funciona e como participar

É formado um grupo para estudar inteligência artificial. Enquanto o grupo estuda, compartilha conhecimentos e troca experiências, 
é proposto um desafio para testar as habilidades e conhecimentos adquiridos. O desafio deve ser interessante de assistir e observar. 
Não pode ser somente dados nem somente textual. Deve ter muitos efeitos visuais e ser atrativo aos olhos.

A base do desafio, um jogo por exemplo, é desenvolvido para que os participantes possam elaborar suas soluções. 
Em uma data estipulada os jogadores com suas propostasa se reúnem e as testam para ver quem conseguiu o melhor resultado. 
Durante a competição é estimulado muita discussão sobre a implementação à base de comida e **cerveja**.

1. Ingresse no grupo
2. Participe das discussões
3. Ajude a desenolver o desafio
4. Baixe o desafio 
5. Desenvolva o BOT
6. Compartilhe a solução
7. Compita presencialmente e socialize :)

#### Ingresse ao grupo

O grupo, por enquanto, é fechado. Para participar, peça para ingressar no grupo no gitlab: https://gitlab.com/aichallenge/
Assim que a solicitação for aceita nós ajudaremos com os próximos passos.

O ingresso no grupo é para participar de discussões fechadas e participar da competição presencial. 
Se houver interesse e não tiver conseguido entrar no grupo, não se preocupe, ainda é possível participar das 
discussões abertas e particar as habilidades com as mesmas ferramentas utilizadas nos desafios.

#### Participe das discussões

Nossas discussões abertas acontecem aqui(link).

#### Baixe o desafio

O desafio atual, do **Tank** (link), ainda está em desenvolvimento. Mas o demo já pode ser baixado aqui (link)

#### Desenvolva o bot

*Explica como desenvolver; como conectar-se ao jogo; e dá dicas de como implementar uma inteligência para o bot*

#### Compartilhe

*Explica como compartilhar o código desenvolvido e onde pegar o código já compartilhado*

#### Participe

*Dicas de como participar nos grupos e se  envolver mais no projeto*

## FAQ

### Onde está acontecendo

Hoje, esse desafio acontece na grande ***Florianópolis***, em Santa Catarina. 
Se você têm interesse em lançar o desafio na sua cidade, ou organizar algum outro, entre em contato conosco pelas redes sociais.

![Floripa](/about/where_floripa.jpg#centered)

### Como faço para participar

Para participar do grupo fechado, peça para entrar no grupo https://gitlab.com/aichallenge/ no gitlab.
Para participar do grupo e discussões abertas, entre em (link)

### Como posso acompanhar

Facebook, gitlab (página e projetos), twitter 

### Que conhecimentos devo ter

Qualquer entusiasta, com algum conhecimento de **alguma linguagem programação** pode parcipar. 
O importante é ter tempo para dedicar-se aos estudos e implementar alguma solução. 
Mas não se preocupe, **o grupo ajuda** a trilhar o caminho.

### Quem participa

O grupo, nesse momento, é bastante pequeno e é composto por (Nome seguido por usuário do gitlab): 

* **Bruno Schneider** *@brunoleos*
* **Filipi Machado dos Santos** *@filipimsantos*
* **Gustavo Brito** *@gustavo-brito*
* **Jean** *@jeanpchr*
* **José João Junior** *@josejoaojunior-jj*
* **Joshua Raposa** *@dalton-e*
* **Matheus Teixeira Fernandes** *@z3r0_th*
* **Rafael** *@rasertux*
* **Salânio Júnior** *@salaniojr*
* **Sandro Dinnebier** *@sandro.dinnebier*

## Desafio Atual

O próximo desafio será criar uma inteligência capaz de ganhar dos outros adversários em um jogo de Tank(link). 
O jogo foi desenvolvido pelo grupo e o código fonte está disponível aqui(link).
Os participantes deverão desenvolver seus bots, que controlará um de quatro tanks. Para saber mais sobre o desafio acesse o link

## Dasafios anteriores

Esse é **o primeiro** desafio e esperamos que ocorra até, no máximo, final de março. O jogo base (dos Tanks) ainda está em desenvolvimento, mas deve estar pronto até o início de fevereiro. =)
